import asyncore
import socket
import logging
import time
import os
import random
import threading

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S') 

HOST = os.getenv("NAME","localhost")
PORT_RANGE = range(55568, 55578)

#Modification from https://github.com/bradjasper/python-p2p

# Client Methods. Used to start comunication
# Since the port is iterable, we need to have IP from UI or server Table 

class EchoClient(asyncore.dispatcher):
    def __init__(self, host, port_range):
        asyncore.dispatcher.__init__(self)
        self.host = host
        self.port = port_range[0]
        self.port_range = port_range
        self.buffer = None
        
    #Bind connection to server
    def reconnect(self):
        self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
        for port in self.port_range:
            try:
                self.log("Attempting to connect to %s:%d" % (self.host, port))
                self.connect((self.host, port))
                self.port = port
                self.log("Looks like host is available")
                return
            except Exception as e:
                self.log("Error connecting to %s:%d" % (self.host, port))

    #Report connection
    def handle_connect(self):
        self.log("Successfully connected to %s:%d" % (self.host, self.port))

    #Check if message is valid/can be send
    def writeable(self):
        return (len(self.buffer) > 0)

    #Shows response from Server
    def handle_read(self):
        data = self.recv(1024)
        if data:
            self.log("Received '%s' from SERVER" % (data,)) 
    
    #Sends msg to Server
    def handle_write(self, msg):
        self.buffer = msg.encode('utf-8')
        if self.buffer:
            self.log("Sending '%s' to SERVER" % self.buffer)
            sent = self.send(self.buffer)
            self.buffer = self.buffer[sent:]

    def handle_close(self, closeTag=False):
        self.close()
        #Close only if want to be closed
        if (closeTag):
            self.host = None
            self.port = None

    def log(self, msg):
        logging.info("CLIENT: %s", msg)

class EchoHandler(asyncore.dispatcher_with_send):
    def handle_read(self):
        data = self.recv(8192)
        if data:
            logging.info("SERVER: Received '%s' from CLIENT" % data)

class EchoServer(asyncore.dispatcher):
    def __init__(self, host, port_range):
        asyncore.dispatcher.__init__(self)
        self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
        self.host = host
        self.set_reuse_addr()
        for port in port_range:
            if self.attempt_connect(port):
                break
        self.listen(5)

    def attempt_connect(self, port):
        try:
            self.bind((self.host, port))
            self.port = port
            self.log("Server found open port and launched on %s:%d" % (self.host, port))
            return True
        except OSError:
            self.log("Port %d is already being used" % (port,))
        except:
            print("Exception")

        return False

    #Handle accept is used for answer
    def handle_accept(self):
        self.log("Handle_Accept")
        pair = self.accept()
        if pair is not None:
            sock, addr = pair
            self.log("Incoming connection from %s" % repr(addr))
            handler = EchoHandler(sock)

    def log(self, msg):
        logging.info("SERVER: %s", msg)

# Keep a list of Threads:
# One for Server or reception
# Host is local IP, and portRange is ports available for reception
class Server (threading.Thread):
    def __init__(self, host,portRange):
      threading.Thread.__init__(self)
      self.host=host
      self.portRange=portRange
    def run(self):
        self.server = EchoServer(self.host, self.portRange)
        asyncore.loop()

#First, create a background thread to recieve messages
servidor = Server("localhost",range(55568, 55578))
servidor.start()
#Then, execute thing using root Thread
#For test purposes, we build up a menu
#Create Client. Host and port to send the message. First port of range accept taken
client = EchoClient("localhost",range(55568, 55578))
while True:
    print("Ingrese la opcion deseada: ")
    print("1.- Enviar mensaje")
    print("2.- Esperar 5 segundos")
    menu = input(' ')
    if (menu=='1'):
        try:
            #Send things
            client.handle_write(msg="Hola from {}".format(1))
            #time.sleep(1)
            client.handle_close() 
        except:
            #If fails (i.e. Connection closed)
            client.reconnect()
            #Send things
            client.handle_write(msg="Hola from 2\n")
            client.handle_write(msg="Segundo mensaje from {}".format(2))
            client.handle_close() 
        #Disconnect (need disconect to write again)
                  
    elif (menu=='2'):
        time.sleep(5)
    elif (menu=='3'):
        exit(0)
