FROM python:3-stretch
WORKDIR /usr/src/app
COPY requirements.txt ./
EXPOSE 44444
EXPOSE 44445
RUN pip install --no-cache-dir -r requirements.txt
COPY . .
CMD python $app